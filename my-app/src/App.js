import Header from "./components/SubMenu";
import {Route, Switch} from "react-router-dom";
import {
    BrowserRouter as Router,
} from "react-router-dom";
import React from "react";
import Home from "./components/Home";
import About from "./components/About";
import Page404 from "./components/Page404";
import Footer from "./components/Footer";
import Blogs from "./components/Blogs";
import Contacts from "./components/Contacts";

function App() {

    return (
        <div className="App">
            <Router>
                <Header />
                <Switch>
                    <Route
                        exact
                        path={"/"}
                        component={Home}
                    />
                    <Route
                        exact
                        path={"/about"}
                        component={About}
                    />
                    <Route
                        exact
                        path={"/blog"}
                        component={Blogs}
                    />
                    <Route
                        exact
                        path={"/contacts"}
                        component={Contacts}
                    />
                    <Route
                        exact
                        path={"*"}
                        component={Page404}
                    />
                </Switch>
            </Router>

        </div>
    );
}

export default App;
