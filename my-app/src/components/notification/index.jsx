import React, {useEffect} from 'react';
import {Snackbar} from "@mui/material";
import {Alert} from "@material-ui/core";
import "./index.css";


const Notification = ({notification, clearNotification}) => {
    useEffect(() => {
        let timerId;
        if (notification) {
            timerId = setTimeout(() => {
                clearNotification();
            }, 5000);
        }
        return () => clearTimeout(timerId);
    }, [notification, clearNotification])

    return (
        <Snackbar
            anchorOrigin={{vertical: 'top', horizontal: 'right'}}
            open={!!notification?.message}
            onClose={clearNotification}
            className="Notification"
        >
            <Alert onClose={clearNotification}>
                <div className="NotificationContent">
                    <div className="NotificationTitle">{notification?.title ?? ""}</div>
                    <div className="NotificationMessage">{notification?.message ?? ""}</div>
                </div>
            </Alert>
        </Snackbar>
    );
};

export default Notification;