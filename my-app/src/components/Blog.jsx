import React from 'react';
import "./blog.css";
import music from './img/music.png'
import forest from './img/forest.jpeg'

const Blog = () => {
    return (
        <div className="blog">
            <img
                className="blogImg"
                src={forest}
            />
            <div className="blogInfo">
                <div className="blogCats">
                    <span className="blogCat">It's My</span>
                    <span className="blogCat">Life</span>
                </div>
                <span className="blogTitle">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                </span>
                <hr/>
                <p className="blogDesc">
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
                    nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                    in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur. Excepteur sint occaecat cupidatat
                    non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
                </p>
            </div>
        </div>
    );
};

export default Blog;