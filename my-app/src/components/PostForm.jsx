import React, {useState} from 'react';
import MyInput from "./UI/input/MyInput";
import MyButton from "./UI/button/MyButton";
import "./index.css";


const PostForm = ({create}) => {

    // const [name, setName] = useState('')
    // const [errorName, setErrorName] = useState('')
    // const [surname, setSurname] = useState('')
    const [subject, setSubject] = useState('')
    const [errorSubject, setErrorSubject] = useState('')
    const [threeMark, setThreeMark] = useState(null)
    const [fourMark, setFourMark] = useState(null)
    const [allSubject, setAllSubject] = useState(null)
    const [fourMarkErr, setFourMarkErr] = useState("")
    const [threeMarkErr, setThreeMarkErr] = useState("")
    const [fiveMarkErr, setFiveMarkErr] = useState("")
    const [fiveMark, setFiveMark] = useState(null)
    const [missed, setMissed] = useState(null)
    const [missedErr, setMissedErr] = useState("")



    const addNewUser = (e) => {
        e.preventDefault()
        if (!subject) {
            setErrorSubject("Введите предмет")
            return;
        } else if (threeMark === null) {
            setThreeMarkErr("Введите количество 3")
            return;
        } else if (fourMark === null) {
            setFourMarkErr("Введите количество 4")
            return;
        } else if (fiveMark === null) {
            setFiveMarkErr("Введите количество 5")
            return;
        } else if (missed === null) {
            setMissedErr("Введите кол-во пропущенных занятий")
            return;
        } else if (missed > allSubject){
            setMissedErr("Ошибка в количестве")
            return;
        }

        const newUser = {
            id: Date.now(),
            // name,
            // surname,
            subject,
            threeMark,
            fourMark,
            fiveMark,
            missed,
            allSubject
        }
        create(newUser)
        // setName('')
        // setSurname('')
        setSubject('')
        setFourMark('')
        setThreeMark('')
        setFiveMark('')
        setMissed('')
        setErrorSubject('')
        setAllSubject('')
    }

    console.log(missed, allSubject)
    return (
    <form>
        {/*<MyInput*/}
        {/*    value={name}*/}
        {/*    onChange={e => {*/}
        {/*        setName(e.target.value)*/}
        {/*        setErrorName("")*/}
        {/*    }}*/}
        {/*    type="text"*/}
        {/*    placeholder="Имя"*/}
        {/*    helperText={errorName}*/}
        {/*    error={!!errorName}*/}
        {/*/>*/}
        {/*<MyInput*/}
        {/*    value={surname}*/}
        {/*    onChange={e => setSurname(e.target.value)}*/}
        {/*    type="text"*/}
        {/*    placeholder="Фамилия"*/}
        {/*/>*/}
        <MyInput
            value={subject}
            onChange={e => {
                setSubject(e.target.value)
                setErrorSubject("")
            }
            }
            helperText={errorSubject}
            error={!!errorSubject}
            type="text"
            placeholder="Предмет"

        />
        <MyInput
            value={threeMark}
            onChange={e => {
                setThreeMark(e.target.value)
                setThreeMarkErr("")
            }}
            helperText={threeMarkErr}
            error={!!threeMarkErr}
            type="number"
            placeholder="Кол-во 3"
        />
        <MyInput
            value={fourMark}
            onChange={e => {
                setFourMark(e.target.value)
                setFourMarkErr("")
            }}
            helperText={fourMarkErr}
            error={!!fourMarkErr}
            type="number"
            placeholder="Кол-во 4"
        />
        <MyInput
            value={fiveMark}
            onChange={e => {
                setFiveMark(e.target.value)
                setFiveMarkErr("")
            }}
            helperText={fiveMarkErr}
            error={!!fiveMarkErr}
            type="number"
            placeholder="Кол-во 5"
        />
        <MyInput
            value={missed}
            onChange={e => {
                setMissed(e.target.value)
                setMissedErr("")
            }}
            helperText={missedErr}
            error={!!missedErr}
            type="number"
            placeholder="Пропущенные занятия"
        />
        <MyInput
            value={allSubject}
            onChange={e => {
                setAllSubject(e.target.value)
                // setMissedErr("")
            }}
            // helperText={missedErr}
            // error={!!missedErr}
            type="number"
            placeholder="Всего занятий"
        />
        <MyButton onClick={addNewUser}>Добавить</MyButton>
    </form>

    );
};

export default PostForm;