import React from 'react';
import "./blogs.css";
import Blog from "./Blog";

const Blogs = () => {
    return (
        <div className="blogs">
            <Blog/>
            <Blog/>
            <Blog/>
            <Blog/>
            <Blog/>
            <Blog/>
            <Blog/>
            <Blog/>
        </div>
    );
};

export default Blogs;