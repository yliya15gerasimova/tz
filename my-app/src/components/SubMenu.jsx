import React from 'react';

import classes from './Submenu.module.css';
import {Link} from "react-router-dom";


const Header = () => {
    return (

            <div className={classes.dwsMenu}>
                <ul>
                    <li><Link to="/">HOME</Link>
                        <ul>
                            <li><a href="#">Подменю 1</a></li>
                            <li><a href="#">Подменю 1</a></li>
                            <li><a href="#">Подменю 1</a></li>
                        </ul>
                    </li>
                    <li><Link to="/about">ABOUT</Link>
                        <ul>
                            <li><a href="#">Подменю 1</a></li>
                            <li><a href="#">Подменю 1</a></li>
                            <li><a href="#">Подменю 1</a></li>
                        </ul>
                    </li>
                    <li><Link to="/blog">BLOG</Link>
                        <ul>
                            <li><a href="#">Подменю 1</a></li>
                            <li><a href="#">Подменю 1</a></li>
                            <li><a href="#">Подменю 1</a></li>
                            <li><a href="#">Подменю 1</a></li>
                        </ul>
                    </li>
                    <li><Link to="/contacts">CONTACTS</Link>
                        <ul>
                            <li><a href="#">Подменю 1</a></li>
                            <li><a href="#">Подменю 1</a></li>
                            <li><a href="#">Подменю 1</a></li>
                            <li><a href="#">Подменю 1</a></li>
                        </ul>
                    </li>

                </ul>
            </div>

    );
};

export default Header;