// import Table from '@material-ui/core/Table';
import { DataGrid } from '@mui/x-data-grid';
import classes from './BtnStyle.module.css';



const DataTable = ({data, handleCount}) => {

    const columns = [

        // { field: 'name', headerName: 'Имя', width: 150 },
        // { field: 'surname', headerName: 'Фамилия',  width: 150},
        { field: 'subject', headerName: 'Предмет', width: 200 },
        { field: 'threeMark', headerName: 'Кол-во 3', width: 200 },
        { field: 'fourMark', headerName: 'Кол-во 4', width: 200 },
        { field: 'fiveMark', headerName: 'Кол-во 5', width: 200 },
        { field: 'missed', headerName: 'Пропущенные занятия', width: 200 },
        { field: 'allSubject', headerName: 'Всего занятий', width: 200 },
        {
            field: 'btn', headerName: "Получение зачета" , width: 200,
            renderCell: (row) => {
                return <button
                    className={classes.btn}
                    onClick={()=> handleCount(row.row.id) }
                >
                    Рассчитать</button>
            }
        }
    ]


    return (

        <div style={{ height: 400, width: '100%' }}>
            <h1 style={{textAlign: 'center'}}>Список студентов</h1>
            <DataGrid
                columns={columns}
                rows={data}
            />
        </div>
    )
}

export default DataTable;