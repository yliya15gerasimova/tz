import React from "react";
import classes from './myInput.module.css'
import TextField from "@mui/material/TextField";

const MyInput = React.forwardRef((props) => {
    return (
        <TextField className={classes.myInput} {...props}/>
    );
});

export default MyInput;