import React from 'react';
import './about.css';
import girl from './img/girl.jpg'

const About = () => {

    return (
        <div className="about">
            <div className="aboutItem">
                <span className="aboutTitle">ABOUT ME</span>
                <img
                    className="aboutImg"
                    src={girl}
                />
                <p>
                    Lorem ipsum dolor sit amet, consectetur
                    adipiscing elit, sed do eiusmod tempor incididunt
                    ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor
                    in reprehenderit in voluptate velit esse cillum
                    dolore eu fugiat nulla pariatur.
                </p>
                <div className="aboutItem">
                    <span className="aboutTitle">CATEGORIES</span>
                    <ul className="aboutList">
                        <li className="aboutListItem">Life</li>
                        <li className="aboutListItem">Style</li>
                        <li className="aboutListItem">Music</li>
                        <li className="aboutListItem">Sport</li>
                    </ul>
                </div>
            </div>
        </div>
    );
};

export default About;