import React, { useState } from 'react';
import Modal from '@material-ui/core/Modal';
import TextField from '@mui/material/TextField';
import Button from '@material-ui/core/Button';
import './index.css';


// export
 const ModalWindow = (props) => {
    const { open, handleClose } = props;
    const [studentName, setStudentName] = useState("")
    return (
        <Modal
            open={open}
            onClose={handleClose}
            className="Modal"
        >
            <div className="ModalContant">
                <TextField  
                    label="Фамилия" 
                    value={studentName}
                    onChange={(e)=> setStudentName(e.target.value)}
                />
                <Button 
                    variant="outlined"
                >
                    Добавить 
                </Button>
            </div>
        </Modal>
    )
}

export default ModalWindow;