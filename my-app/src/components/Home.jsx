import React from 'react';
import {useState} from 'react';
import DataTable from './Table';
import MyModal from "./MyModal/MyModal";
import PostForm from "./PostForm";
import MyButton from "./UI/button/MyButton";
import Notification from "./notification";

const Home = () => {

    const [table, setTable] = useState([
        {
            id: '1',
            subject: "Russian language",
            threeMark: 3,
            fourMark: 4,
            fiveMark: 5,
            missed: 9,
            allSubject: 10
        },
        {
            id: '2',
            subject: "math",
            threeMark: 2,
            fourMark: 6,
            fiveMark: 5,
            missed: 3,
            allSubject: 13
        },
    ])
    const [modal, setModal] = useState(false)
    const [notification, setNotification] = useState(null)

    const createUser = (newUser) => {
        setTable([...table, newUser])
        setModal(false)
    }

    const handleCount = (id) => {
        const foundSubject = table.find(el=> el.id === id)
        if (((
            (3 * foundSubject.threeMark + 4 * foundSubject.fourMark + 5 * foundSubject.fiveMark)
            /
            (foundSubject.threeMark + foundSubject.fourMark + foundSubject.fiveMark)
        ) > 4) && (((foundSubject.missed / foundSubject.allSubject) * 100) < 10)) {
            setNotification({
                title: "Уведомление",
                message: "Вы получите автомат"
            })
        } else {
            setNotification({
                title: "Уведомление",
                message: "Вы НЕ получите автомат"
            })
        }
    }
    return (
        <div>
            <MyButton style={{marginTop: "20px"}} onClick={() => setModal(true)}>
                Создать пользователя
            </MyButton>
            <MyModal visible={modal} setVisible={setModal}>
                <PostForm create={createUser}/>
            </MyModal>
            <DataTable
                handleCount={handleCount}
                data={table}
            />
            <Notification
                notification={notification}
                clearNotification={() => setNotification(null)}
            />
        </div>
    );
};

export default Home;